# OpenML dataset: miami_housing

https://www.openml.org/d/44983

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Data description**

The dataset contains information on 13,932 single-family homes sold in Miami .
Content.

The goal is to predict the sale price.

**Attribute description**

The dataset contains the following columns:

  * PARCELNO: unique identifier for each property. About 1% appear multiple times.
  * SALE_PRC: sale price ($)
  * LND_SQFOOT: land area (square feet)
  * TOTLVGAREA: floor area (square feet)
  * SPECFEATVAL: value of special features (e.g., swimming pools) ($)
  * RAIL_DIST: distance to the nearest rail line (an indicator of noise) (feet)
  * OCEAN_DIST: distance to the ocean (feet)
  * WATER_DIST: distance to the nearest body of water (feet)
  * CNTR_DIST: distance to the Miami central business district (feet)
  * SUBCNTR_DI: distance to the nearest subcenter (feet)
  * HWY_DIST: distance to the nearest highway (an indicator of noise) (feet)
  * age: age of the structure
  * avno60plus: dummy variable for airplane noise exceeding an acceptable level
  * structure_quality: quality of the structure
  * month_sold: sale month in 2016 (1 = jan)
  * LATITUDE
  * LONGITUDE

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44983) of an [OpenML dataset](https://www.openml.org/d/44983). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44983/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44983/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44983/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

